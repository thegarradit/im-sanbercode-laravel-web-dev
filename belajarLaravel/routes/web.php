<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::get('/Register', [AuthController::class, 'form']);
Route::post('/Welcome', [AuthController::class, 'kirim']);
Route::get('/table', function(){
    return view('table');
});
Route::get('/data-table', function(){
    return view('data-table');
});

// Route::get('/cast', function(){
//     return view('Casting.daftar');
// });

// CRUD

// Create Data
// Route ke tambah data
Route::get('/cast/create', [CastController::class, 'create']);
// Route Simpan Data
Route::post('/cast', [CastController::class, 'store']);

// Read Data
// Route tampil daftar
Route::get('/cast', [CastController::class, 'index']);
// Route Detail Cast
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

// Update Data
// Update Data Id tertentu
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
// menampilkan data update
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

// Delete Data
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);
