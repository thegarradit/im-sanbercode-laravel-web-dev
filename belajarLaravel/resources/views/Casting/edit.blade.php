@extends('layout.master')

@section('title')
    Create Cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label for="exampleInputEmail1">Nama</label>
      <input type="text" class="form-control @error('title') is-invalid @enderror" value="{{$cast->nama}}" name="nama" aria-describedby="emailHelp">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="exampleInputPassword1">Umur</label>
      <input type="number" class="form-control @error('title') is-invalid @enderror" name="umur" value="{{$cast->umur}}">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="exampleInputEmail1">Bio</label> <br>
        <textarea class="form-control @error('title') is-invalid @enderror" name="bio" cols="30" rows="10">"{{$cast->bio}}"</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
