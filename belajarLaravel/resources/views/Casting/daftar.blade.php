@extends('layout.master')

@section('title')
    Daftar Cast
@endsection

@section('content')
<br>
<table class="table table-striped table-bordered ">
    <thead class="thead-dark    ">
        <tr>
            <th scope="col">id</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $value)
        <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$value->nama}}</td>
            <td>{{$value->umur}}</td>
            <td>
                <form action="/cast/{{$value->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{$value->id}}" class="btn btn-primary btn-sm">Detail</a>
                    <a href="/cast/{{$value->id}}/edit" class="btn btn-info btn-sm">Perbarui</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </td>
        </tr>
        @empty
            <tr>
                <td>No data</td>
            </tr>
        @endforelse
    </tbody>
</table>
<br>
<a href="/cast/create" class="btn btn-light btn-sm my-3">Tambah Cast Baru</a>
@endsection