@extends('layout.master')

@section('title')
    Tugas 1 Berlatih HTML
@endsection

@section('content')
    <h1>Buat Account Baru!!!</h1>

    <h3>Sign Up Form</h3>

    <form action="/Welcome" method="POST">
        @csrf
        <label for="">First Name:</label> <br><br>
        <input type="text" name="firstName"> <br><br>
        <label>Last  Name:</label> <br><br>
        <input type="text" name="lastName"> <br><br>
        <label>Gender:</label> <br><br>
        <input type="radio">Male <br>
        <input type="radio">Female <br>
        <input type="radio">Other <br><br>
        <label>Nationality:</label> <br><br>
        <select name="Nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Javanese">Javanese</option>
            <option value="Other">Other</option>
        </select> <br><br>
        <label>Languange Spoken:</label> <br><br>
        <input type="checkbox"> Indonesian <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Laennya <br><br>
        <label>Bio:</label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea>
        <br><br>

        <input type="submit" value="Sign Up"> 
    </form>
@endsection
