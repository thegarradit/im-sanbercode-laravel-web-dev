<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index(){
        $cast = DB::table('cast')->get();
 
        return view('Casting.daftar', ['cast' => $cast]);
    }

    public function create(){
        return view('Casting.tambah');
    }

    public function store(Request $request){

        $validated = $request->validate([
            'nama' => 'required|min:2',
            'umur' =>'required',
            'bio' => 'required',
        ]);
        DB::table('cast')->insert([
            'nama' => $request->input('nama'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio')
        ]);
        
        return redirect('/cast');
    }

    public function show($id){
        $cast = DB::table('cast')->find($id);

        return view('Casting.detail', ['cast' => $cast]);
    }

    public function edit($id){
        $cast = DB::table('cast')->find($id);

        return view('Casting.edit', ['cast' => $cast]);
    }

    public function update($id, Request $request){
        $validated = $request->validate([
            'nama' => 'required|min:2',
            'umur' =>'required',
            'bio' => 'required',
        ]);
        DB::table('cast')
        ->where('id', $id)
        ->update([
            'nama' => $request->input('nama'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio'),
        ]);

        return redirect('/cast');
    }

    public function destroy($id){
        DB::table('cast')->where('id', '=', $id)->delete();

        return redirect('/cast');
    }
}
