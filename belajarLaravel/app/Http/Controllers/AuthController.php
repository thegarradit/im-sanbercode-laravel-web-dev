<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('Register');
    }

    public function kirim(Request $request){
        // dd($request);
        $namaDepan = $request -> input('firstName');
        $namaBelakang = $request -> input('lastName');

        return view('Welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
