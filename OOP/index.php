<?php
    require_once("Animal.php");
    require_once("Ape.php");
    require_once("Frog.php");

    $Sheeps = new Animal("shaun");
    echo "Name : " . $Sheeps -> name . "<br>";
    echo "legs : " . $Sheeps -> legs . "<br>";
    echo "Cold Blooded : " . $Sheeps -> cold_blooded . "<br>";

    echo "<br>";

    $kodok = new Frog("buduk");
    echo "Name : " . $kodok -> name . "<br>";
    echo "legs : " . $kodok -> legs . "<br>";
    echo "Cold Blooded : " . $kodok -> cold_blooded . "<br>";
    echo "Jump : " . $kodok -> jump() . "<br>";

    echo "<br>";

    $Kera = new Ape("Kera Sakti");
    echo "Name : " . $Kera -> name . "<br>";
    echo "legs : " . $Kera -> legs . "<br>";
    echo "Cold Blooded : " . $Kera -> cold_blooded . "<br>";
    echo "Yell : " . $Kera -> yell() . "<br>";


?>