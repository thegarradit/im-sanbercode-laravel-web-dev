<?php
    require_once("Animal.php");

    class Ape extends Animal 
    {
        public $legs = 2;
        // public $Yell = "Auooo";

        public function yell() {
            return "Auououuouououou";
        }
    }
?>